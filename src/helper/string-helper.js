export const upperCaseFirstLetter = str => 
  `${str.charAt(0).toUpperCase()}${str.slice(1)}`;

export const replaceAll = (str, value, replacement) => {
  const regex = new RegExp(value, "g");
  return str.replace(regex, replacement);
}