const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    proxy({
      target: 'https://5zydb.sse.codesandbox.io',
      changeOrigin: true,
    }),
  );
};
