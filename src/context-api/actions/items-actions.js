export const fetchItemsAction = async ({ url, take, skip }) => {
  let fullUrl = `${url}/items`;

  if(take || skip){
    fullUrl += buildQueryParams({ take, skip });
  }

  try {
    const res = await window.fetch(fullUrl);

    if(!res.ok) {
      return {
        items: [],
        errorCode: res.status,
        errorMsg: res.statusText
      };
    }


    const resAsJson = await res.json();

    return {
      items: resAsJson,
      errorCode: null,
      errorMsg: null
    };
  } catch (e) {
    return {
      items: [],
      errorCode: 500,
      errorMsg: 'Internal Server Error'
    };
  }
};

function buildQueryParams( params ) {
  let queryParamsStr = '?';

  for (let key in params){
    if(params[key]){
      queryParamsStr += `&${key}=${params[key]}`;
    }
  }
  return queryParamsStr;
}