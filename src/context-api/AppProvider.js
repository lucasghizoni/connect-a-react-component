import React, { useState, createContext, useContext, useMemo } from 'react';
import { fetchItemsAction } from './actions/items-actions';

const Context = createContext();

export const AppProvider = props => {
  const [state, setState] = useState({
    items: [],
    fetchItemsStatus: {
      error: false,
      errorMsg: '',
    },
    authorized: true // lets suppose user is loggedin
  });

  const context = useMemo(() => {
    return {
      state,
      actions: {
        fetchItems: async ({ url, take, skip }) => {
            const { errorCode, errorMsg, items } = await fetchItemsAction({ url, take, skip });
            
            const authorized = errorCode !== 401;

            const newItems = 
              errorCode ?
                []
                : [...state.items, ...items ];

            setState( prevState => ({
              ...prevState,
              fetchItemsStatus: {
                errorMsg,
                error: errorCode !== null,
              },
              authorized,
              items: newItems,
            }));
          }
        },
      };
  }, [ state ]);

  return <Context.Provider value={context}>{props.children}</Context.Provider>;
};

export const useAppContext = () => useContext(Context);
