import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from './context-api/AppProvider';
import 'normalize.css';

import './styles.css';
import ItemsComponent from './items-component';

const SERVER_BASE_URL = 'https://5zydb.sse.codesandbox.io';

/**
 * This component is part of the boilerplate.
 * You don't need to modify it.
 *
 * PS: Only modified to provide the context-api
 */
function AppWithContext() {
  return (
    <AppProvider>
      <ItemsComponent
        restEndpoint={'/api'}
        graphqlEndpoint={SERVER_BASE_URL + '/graphql'}
      />
    </AppProvider>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<AppWithContext />, rootElement);
