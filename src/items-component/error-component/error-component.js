import React from 'react';
import './error-component.css';

export default function ErrorComponent({ msg, action, actionLabel }) {
  return (
    <div className="ErrorComponent">
      <h4 className="ErrorComponent-msg">{msg}</h4>
      <button 
        className="btn btn-danger" 
        onClick={action}
      >
        {actionLabel}
      </button>
    </div>
  );
}