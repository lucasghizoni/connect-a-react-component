import React from 'react';

export default function Footer({ onLoadMore }) {
  return (
    <div className="Footer">
      <button className="btn btn-primary" onClick={onLoadMore}>Load more</button>
    </div>
  );
}