import React from 'react';
import TableRowCell from './table-row-cell';

export default function TableRow({ item, columns }) {
  const itemKeys = Object.keys(item);

  const renderItem = key => {
    const colConfig = columns[key];

    if(!colConfig) {
      console.warn('Column config not found for item: ', item[key]);
      return null;
    }

    return (
      <TableRowCell
        content={item[key]}
        format={colConfig.format}
      />
    );
  };

  return (
    <tr>
      {itemKeys.map(renderItem)}
    </tr>
  )
}