import React from 'react';

export default function TableRowCell({ content, format }) {
  const displayValue = format ? format(content) : content;

  return (
    <td>{displayValue}</td>
  );
}