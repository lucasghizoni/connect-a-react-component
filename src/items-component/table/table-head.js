import React from 'react';

export default function TableHead({ columns }) {
  const columnsKeys = Object.keys(columns);
  
  const renderItem = item => {
    const currentItem = columns[item];
    return (
      <th key={currentItem.id}>
        {currentItem.label}
      </th>
    );
  };

  return (
    <thead>
      <tr>
        {columnsKeys.map(renderItem)}
      </tr>
    </thead>
  );
};
