import React from 'react';
import './table.css';
import TableHead from './table-head';
import TableRow from './table-row';
import TableFooter from './table-footer';

export default function Table({ columns, items, onLoadMore }) {
  const renderItem = item => (
    <TableRow
      key={item.name} 
      item={item}
      columns={columns}
    />
  );

  return (
    <>
      <div className="Table-container">
        <table>
          <TableHead columns={columns}/>
          <tbody>
            {items.map(renderItem)}
          </tbody>
        </table>
      </div>
      <TableFooter onLoadMore={onLoadMore}/>
    </>
  );
}