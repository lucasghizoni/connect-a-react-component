import React from 'react';
import './authenticate.css';

import { useAppContext } from './../../context-api/AppProvider';

export default function Authenticate({ children }) {
  const { authorized } = useAppContext().state;

  const handleClick = () => {
    window.location.reload();
  }

  if(authorized) {
    return children;
  }

  return (
    <div className="Authenticate">
      <div className="Authenticate-warnMsg">
        <h2>You are not logged in.</h2>
        <h3>Please log in first.</h3>
      </div>
      <div className="Authenticate-reloadContainer">
        <h5>Or try to do one more request to the server and pray for your authorization:</h5>
        <button onClick={handleClick} className="btn btn-primary">Reload</button>
      </div>
    </div>
  );
}