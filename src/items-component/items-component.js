import React, { useEffect, useState } from 'react';
import './items-component.css';

import { useAppContext } from './../context-api/AppProvider';
import Table from './table';
import Authenticate from './authenticate';
import ErrorComponent from './error-component';
import { germanFormat } from '../helper/date-helper';
import { upperCaseFirstLetter, replaceAll } from '../helper/string-helper';

const PAGE_SIZE = 10;
const COLUMNS = {
  name: {
    label: 'Name',
    format: value => {
      const strUpperCase = upperCaseFirstLetter(value);
      return replaceAll(strUpperCase, '_', ' '); 
    }
  },
  created: {
    label: 'Created',
    format: value => germanFormat(value)
  }
};

export default function ItemsComponent({ restEndpoint }) {
  const { state, actions } = useAppContext();
  const [ listControls, setListControls ] = useState({ skip: 0, reload: false });
  
  const { items, fetchItemsStatus } = state;
  const { errorMsg, error } = fetchItemsStatus;

  useEffect(() => {
    actions.fetchItems({ 
      url: restEndpoint, 
      take: PAGE_SIZE,
      skip: listControls.skip 
    });
  }, [ listControls ]);

  const handleLoadMore = () => {
    setListControls({
      ...listControls,
      skip: listControls.skip + PAGE_SIZE
    });
  };

  const handleReload = () => {
    setListControls({
      reload: !listControls.reload,
      skip: 0
    });
  };

  return (
    <Authenticate>
      <div className="ItemsComponent-container">
        <div className="ItemsComponent-title">
          <h2>
            List of items
          </h2>
        </div>
        {!error &&
            <Table
              columns={COLUMNS}
              items={items}
              onLoadMore={handleLoadMore}
            />
        }
        {error &&
          <ErrorComponent
            msg={errorMsg}
            action={handleReload}
            actionLabel="Reload"
          />
        }
      </div>
    </Authenticate>
  );
}
