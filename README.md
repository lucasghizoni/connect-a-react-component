# Connect a react Component

## Running the application

```sh
npm install
```

and then:
```sh
npm start
```

## Main Components

### <Authenticate/>

A component that checks in the global state if the user is authenticated or not. If true, it will show the children components. If not, It will render a page indicating that user is not authenticated.

### <Table/>

A component to render items in a list. It is possible to configure with columns, and also formatters for each content. Inside this component, it also contains other components that abstracts some parts of a table, like <TableRow/>, <TableFooter/>, <TableHead/> and <TableRowCell/>.

### <ErrorComponent/>
Simple component that shows and error message, and a button with an action.

## Context API
For sharing state in the app, the react context-api was used. The <AppProvider> is defined on the top of the App, and down the tree it is possible to access the context, just calling the method useAppContext(). 